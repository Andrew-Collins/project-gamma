### 11/14/2022
Last week was spend working as team to define the wire frame of our application. The general functionally and stretch goals were defined.

Current stretch goals are:
* Saving Gifts as favorites for later
* Email Notifications on upcoming events
* Calendar view included with list of events

Link to our current working excalidraw:
[Largesseance](https://excalidraw.com/#room=7e097e63c1fb339686de,p3bUaSu65jLaPaDdIrvEag)

I spent some time translating our API endpoints from the excalidraw into the markdown file located at `\docs\api-design.md`
